struct Node {
	int data;
	struct Node *next;
};

void addBegin(struct Node **head, int data) {
	struct Node *newNode = (struct Node *) malloc(sizeof(struct Node));
	
	newNode->data = data;
	newNode->next = *head;
	*head = newNode;
}

void addEnd(struct Node **head, int data) {
	struct Node *newNode, *curNode;
	newNode = (struct Node *) malloc(sizeof(struct Node));
	newNode->data = data;
	curNode = *head;
	
	// insert directly if head not assigned
	if (NULL == *head) {
		newNode->next = *head;
		*head = newNode;
	} else {
	// traverse to last node
		while (NULL != curNode->next) {
			curNode = curNode->next;
		}
	
		// append at last node
		newNode->next = NULL;
		curNode->next = newNode;
	}
}

void addAt(struct Node **head, int data, int location) {
	struct Node *newNode, *curNode, *prevNode;
	
	if (location > length(*head)) {
		puts("index too high for list. cannot insert here");
	} else if (location < 1) {
		puts("index must be a positive, non-zero integer");
	} else {
		if (location == 1) {
			addBegin(head, data);
		} else {
			curNode = *head;
			
			int i;
			for (i = 1; i < location; i++) {
				prevNode = curNode;
				curNode = curNode->next;
			}
			
			newNode = (struct Node *) malloc(sizeof(struct Node));
			newNode->data = data;

			prevNode->next = newNode;
			newNode->next = curNode;
		}
	}
}

void deleteBegin(struct Node **head) {
	if (NULL == head) {
		// do nothing
		return;
	}

	struct Node *firstNode = *head;
	*head = firstNode->next;
	free(firstNode);
}

void deleteEnd(struct Node **head) {
	struct Node *curNode, *prevNode;
	curNode = *head;
	
	if (NULL == *head) {
		// do nothing
		return;
	}
	
	// traverse to end of list
	while (NULL != curNode->next) {
		prevNode = curNode;
		curNode = curNode->next;
	}
	
	prevNode->next = NULL;
	free(curNode);
}

void deleteAt(struct Node **head, int location) {
	struct Node *curNode, *prevNode;
	int curLocation = 1;
	curNode = *head;
	
	if (location < 1) {
		puts("location must be greater than 0");
		return;
	} else if (location == 1) {
		deleteBegin(head);
		return;
	} else if (location > length(*head)) {
		puts("location greater than length of list");
		return;
	}

	while (location > curLocation) {
		prevNode = curNode;
		curNode = curNode->next;
		curLocation++;
	}

	prevNode->next = curNode->next;
	free(curNode);
}

int length(struct Node *head) {
	struct Node *curNode = head;
	int count = 0;

	while (curNode != NULL) {
		curNode = curNode->next;
		count++;
	}

	return count;
}
