#include <stdio.h>
#include <stdlib.h>
#include "intNode.h"

int main(void) {
	
	struct Node *head = NULL;

	addEnd(&head, 15);
	addEnd(&head, 25);
	addBegin(&head, 14);
	addAt(&head, 22, 1);
	addAt(&head, 30, 4);
	addAt(&head, 30,4);

	struct Node *itr = head;
	while (itr != NULL) {
		printf("%d\n", itr->data);
		itr = itr->next;
	}
	puts("");

	deleteAt(&head, 6);
	
	itr = head;
	while (itr != NULL) {
		printf("%d\n", itr->data);
		itr = itr->next;
	}

	return 0;
}
